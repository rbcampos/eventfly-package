import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

const Btn = styled.button`
  border: medium none;
  border-spacing: 0;
  height: 48px;
  width: ${props => {
    if (props.large) {
      return '244px'
    } else if (props.small) {
      return '100px'
    } else if (props.extraLarge) {
      return '300px'
    }
    return '175.23px'
  }};
  border-radius: ${props => (props.retangle ? '6px' : ' 100px')};
  background: ${props => {
    if (props.gray) {
      return 'linear-gradient(0deg, rgba(255,255,255,0.8) 0%, #FFFFFF 100%)'
    } else if (props.blue) {
      return '#4A90E2'
    }
    return 'linear-gradient(180deg, rgba(191, 45, 151, 0.8) 0%, #bf2d97 100%)'
  }};
  font-family: Museo;
  color: ${props => {
    if (props.gray) {
      return '#BF2D97'
    } else if (props.blue) {
      return '#F2F2F2'
    }
    return '#FFFFFF'
  }};
  font-size: 17px;
  line-height: 24px;
  text-align: center;
`
const Button = ({ children, url, onClick, ...props }) => (
  <Btn {...props} onCLick={onClick}>
    {url ? <Link to={url}>{children}</Link> : children}
  </Btn>
)

Button.propTypes = {
  children: PropTypes.string,
  large: PropTypes.bool,
  extraLarge: PropTypes.bool,
  small: PropTypes.bool,
  retangle: PropTypes.bool,
  gray: PropTypes.bool,
  blue: PropTypes.bool,
  url: PropTypes.string,
  onClick: PropTypes.func,
}

Button.defaultProps = {
  children: '',
  large: false,
  extraLarge: false,
  small: false,
  retangle: false,
  gray: false,
  blue: false,
  url: '',
  onClick: () => {},
}

Button.types = Object.keys(Button.propTypes)

export default Button
