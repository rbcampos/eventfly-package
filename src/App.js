import React from 'react'
import { render } from 'react-dom'

import Button from './components/Button/Button'

const Demo = () => (
  <div
    style={{
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#eee',
      padding: '100px',
    }}
  >
    <h1>Button</h1>
    <p>Props: {Button.types.toString()}</p>
    <Button>Call to Action</Button>
    <Button gray large>
      label
    </Button>
    <Button retangle blue extraLarge>
      Call to Action
    </Button>
  </div>
)

render(<Demo />, document.getElementById('root'))
